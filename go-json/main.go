package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func data(w http.ResponseWriter, req *http.Request) {
	var m = map[string]string {
		"result" : "OK",
		"data" : "Messages received successfully",
		"transferred" : "10567 bytes",
	}
	res, _ := json.Marshal(m)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, string(res))
}

func main() {
	http.HandleFunc("/data",data)
	http.ListenAndServe("127.0.0.1:8080",nil)
}
